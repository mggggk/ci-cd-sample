FROM ubuntu:20.04
LABEL maintainer "mggggk <mggggk@gmail.com>"

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV TZ Asia/Tokyo
ENV USER_ID 1000
ENV GROUP_ID 1000

RUN set -x \
  && : "Add user docker" \
  && adduser --disabled-login --shell /bin/bash --gecos "" docker \
  && usermod -a -G sudo docker

RUN set -x \
  && : "Install basic app" \
  && apt-get -y update \
  && apt-get -y install cron tzdata

RUN set -x \
  && : "Install optional" \
  && apt-get -y install less

RUN set -x \
  && : "Clean" \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/*

RUN set -x \
  && : "Append bashrc" \
  && touch /root/.bashrc \
  && { \
    echo '# switch user "docker"'; \
    echo 'su docker'; \
    echo 'cd ~/'; \
  } >> /root/.bashrc

# EXPOSE 7777 7777/udp 7778 7778/udp 27015 27015/udp 32330
# EXPOSE 7779 7779/udp 7780 7780/udp 27016 27016/udp 32331

COPY user.sh /app/
COPY run.sh /app/

ENTRYPOINT ["/app/user.sh"]
