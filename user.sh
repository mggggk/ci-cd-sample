#!/bin/sh

# Change the USER_ID if needed
if [ ! "$(id -u docker)" -eq "$USER_ID" ]; then
  echo "Changing docker uid to $USER_ID."
  usermod -o -u "$USER_ID" docker ;
fi
# Change GROUP_ID if needed
if [ ! "$(id -g docker)" -eq "$GROUP_ID" ]; then
  echo "Changing docker GROUP_ID to $GROUP_ID."
  groupmod -o -g "$GROUP_ID" docker ;
fi

# Put docker owner of directories (if the uid changed, then it's needed)
chown -R docker:docker /app/ /home/docker

# Launch run.sh with user docker
su - docker -c "/app/run.sh $@"
